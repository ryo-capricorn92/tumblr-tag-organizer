const TASK_TYPES = {
  ORDER_BY: 'orderBy',
  REPLACE: 'replace',
  REMOVE: 'remove',
  ADD: 'add',
};

module.exports = {
  TASK_TYPES,
};
