const dotenv = require('dotenv');
const path = require('path');

const { askUser } = require('./util');
const { deleteMarked, getDraft } = require('./tools/drafts');
const init = require('./tools/init');

if (process.env.NODE_ENV !== 'production') {
  dotenv.config({ path: path.join(__dirname, '.local.env') });
}

(async () => {
  let tool = process.argv[2];

  if (!tool) {
    tool = await askUser(`
      ** Welcome to the Tumblr Assistant **
      -------------------------------------
      What tool would you like to use?
      - init
      - drafts
      - delete

      If you have not run Tumblr Assistant before, please run init first!!
    `);
  }

  tool = (tool || '').toLowerCase();

  switch (tool) {
    case 'init':
      init();
      break;
    case 'drafts':
      getDraft();
      break;
    case 'delete':
      deleteMarked();
      break;
    default:
      console.log(`Opening ${tool} . . .`); // eslint-disable-line no-console
  }
})();
