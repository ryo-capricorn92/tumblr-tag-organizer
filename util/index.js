const readline = require('readline');

const askUser = (question) => {
  const ui = new readline.Interface({
    input: process.stdin,
    output: process.stdout,
  });

  return new Promise((res) => ui.question(question, (response) => {
    ui.close();
    res(response || '');
  }));
};

const getOgDataFromPost = (post) => {
  let ogPostId;
  let op;
  let source;

  if (post.source_url && post.source_url.includes('tumblr.com')) {
    const matches = /^https:\/\/([a-zA-Z0-9-]+).tumblr.com\/post\/([0-9]+)\/[a-zA-Z0-9-]+$/g
      .exec(post.source_url);

    const [, blog, id] = (matches || []);

    source = blog || source;
    ogPostId = id || ogPostId;
  }

  const opTrail = post.trail.find((t) => t.is_root_item);
  if (!opTrail) {
    op = 'N/A';
  } else if (opTrail.broken_blog_name) {
    op = `${opTrail.broken_blog_name} (deactivated)`;
  } else {
    op = opTrail.blog.name;
    ogPostId = opTrail.post.id;
  }

  return {
    ogPostId,
    op,
    source,
  };
};

const getRandom = (min, max) => Math.floor(Math.random() * (max - min) + min);

const getRandomFromList = (list) => {
  const index = getRandom(0, list.length);
  return list[index];
};

module.exports = {
  askUser,
  getOgDataFromPost,
  getRandom,
  getRandomFromList,
};
