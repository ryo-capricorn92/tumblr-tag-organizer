const { version } = require('../../package.json');

const { TASK_TYPES } = require('../../constants');
// below is a git ignored file - I don't know who you are, but you don't need to know my tumblr tags
// see ./tags.example.js for reference
// the init function will also generate this file
const { orderedTags, replacementPairs } = require('./tags');

/*
  ***** TASK MODELS *****

  ** ORDER BY **
  {
    key: // string - unique identifier for task
    type: TASK_TYPES.ORDER_BY
    tags: // array of strings - list of tags in order they should appear, if present
    exclude: // array of strings - list of tags that should not be included in task
    includeAll: // bool - do all tags need to be present to apply task
    otherTagsFirst: // bool - should unsorted tags come before the sorted tags
    tagMarker: // string - tag to add to any posts changed by this task
  }

  ** REPLACE **
  {
    key: // string - unique identifier for task
    type: TASK_TYPES.REPLACE
    tags: // array of strings - list of tags to delete
    newTags: // array of strings - list of tags to add
    exclude: // array of strings - list of tags that should not be included in task
    includeAll: // bool - do all tags need to be present to apply task
    tagMarker: // string - tag to add to any posts changed by this task
  }
*/

const replacementTasks = replacementPairs.map((replace) => ({
  key: `replace-${replace.old}-with-${replace.new}`,
  type: TASK_TYPES.REPLACE,
  tags: replace.old,
  newTags: replace.new,
  exclude: [],
  includeAll: false,
  tagMarker: `auto updated ${version}`,
}));

module.exports = [
  {
    key: 'orderTags',
    type: TASK_TYPES.ORDER_BY,
    tags: orderedTags,
    exclude: [],
    includeAll: false,
    otherTagsFirst: false,
    tagMarker: `auto organized ${version}`,
  },
  ...replacementTasks,
];
