// these tags will be moved to the front of the tag list in this order
const orderedTags = [
  'a tag', // tag must match exactly
  'a tag with a wildcard *', // tag can have anything in place of the *
];

// these tags will replace any instances of the old tags with the new tags
const replacementPairs = [
  {
    old: ['old tag', 'another tag that should also be replaced'],
    new: ['new tag'],
  },
];

module.exports = {
  orderedTags,
  replacementPairs,
};
