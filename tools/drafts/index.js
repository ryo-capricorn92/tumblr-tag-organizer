const childProcess = require('child_process');
const fs = require('fs');
const path = require('path');
const Tumblr = require('tumblr.js');

const { askUser, getOgDataFromPost, getRandomFromList } = require('../../util');
const { last } = require('./data.json');

const deleteMarked = async () => {
  const client = new Tumblr.Client({
    credentials: {
      consumer_key: process.env.CONSUMER_KEY,
      consumer_secret: process.env.CONSUMER_SECRET,
      token: process.env.TOKEN,
      token_secret: process.env.TOKEN_SECRET,
    },
    returnPromises: true,
  });

  const findPostsToDelete = async (lastId, toDelete = []) => {
    const options = lastId ? { before_id: lastId } : null;
    const { posts } = await client.blogDrafts(process.env.BLOG, options);

    if (!posts.length) { return toDelete; }

    posts
      .filter((p) => p.tags.includes('delete'))
      .forEach((p) => { toDelete.push(p.id); });

    const [lastPost] = posts.slice(-1);

    return findPostsToDelete(lastPost.id, toDelete);
  };

  console.log('Searching . . . '); // eslint-disable-line no-console

  const postsToDelete = await findPostsToDelete();

  const confirm = await askUser(`Found ${postsToDelete.length} draft(s). Delete? (no):`);

  switch (confirm.toLowerCase()) {
    case 'yes':
    case 'y':
      postsToDelete.forEach((id) => {
        client.deletePost(process.env.BLOG, { id });
      });
      console.log('Deleting . . .');
      break;
    default:
      console.log('Canceling.');
  }
};

let currentLast = last;

const getDraft = async () => {
  console.log('------------------'); // eslint-disable-line no-console
  const client = new Tumblr.Client({
    credentials: {
      consumer_key: process.env.CONSUMER_KEY,
      consumer_secret: process.env.CONSUMER_SECRET,
      token: process.env.TOKEN,
      token_secret: process.env.TOKEN_SECRET,
    },
    returnPromises: true,
  });

  const options = currentLast ? { before_id: currentLast } : null;

  const { posts } = await client.blogDrafts(process.env.BLOG, options);
  const post = getRandomFromList(posts);

  const [lastPost] = posts.slice(-1);
  currentLast = lastPost ? lastPost.id : currentLast;

  if (!post) {
    currentLast = null;
    return getDraft();
  }

  const newData = JSON.stringify({
    last: currentLast,
  });
  fs.writeFileSync(path.join(__dirname, 'data.json'), newData);

  const { ogPostId, op, source } = getOgDataFromPost(post);

  console.log(`${op} | ${source}`); // eslint-disable-line no-console
  console.log(`Notes: ${(post.note_count || 0).toLocaleString()}`); // eslint-disable-line no-console

  try {
    const og = await client.blogPosts(source || op, { id: ogPostId });

    if (og && og.posts && og.posts.length) {
      const [{ tags }] = og.posts;
      console.log(`Tags: ${tags.join(', ')}`); // eslint-disable-line no-console
    }
  } catch (err) {
    console.log('Failed to fetch original'); // eslint-disable-line no-console
  }

  childProcess.exec(`open -a "Google Chrome" https://www.tumblr.com/edit/${post.id}`, () => {});

  console.log('------------------'); // eslint-disable-line no-console
  const answer = await askUser('Another? (yes): ');
  switch (answer.toLowerCase()) {
    case '':
    case 'y':
    case 'yes':
      return getDraft();
    default:
      return null;
  }
};

module.exports = {
  deleteMarked,
  getDraft,
};
