const {
  constants: fsConstants,
  copyFileSync,
  readFileSync,
  writeFileSync,
} = require('fs');
const path = require('path');

const generateDataFile = (relativePath) => {
  let data;

  try {
    data = require(relativePath); // eslint-disable-line
  } catch (e) {
    data = {};
  }

  if (!data || typeof data !== 'object') {
    data = {};
  }

  writeFileSync(path.join(__dirname, relativePath), JSON.stringify(data));
};

const generateTemplateFile = (examplePath, destinationPath) => {
  const exAbsPath = path.join(__dirname, examplePath);
  const destAbsPath = path.join(__dirname, destinationPath);

  const commentText = `// This file was created from the template located here: ${exAbsPath}
// Edit it how you'd like for your prefered behavior, but retain the same variable names and types
// (unless you plan on doing some hardcore changes to other files)

`;

  try {
    copyFileSync(exAbsPath, destAbsPath, fsConstants.COPYFILE_EXCL);
    const file = readFileSync(destAbsPath);
    const commented = commentText + file;
    writeFileSync(destAbsPath, commented);
  } catch (e) {
    console.warn(`INIT WILL NOT OVERWRITE EXISTING FILES
    A file already exists at ${destAbsPath}
    If you want to overwrite this file, please delete the existing file first
    (this is expected unless this file is giving you trouble)`);
  }
};

const init = () => {
  generateDataFile('../drafts/data.json');
  generateDataFile('../tagger/data.json');

  generateTemplateFile('../tagger/tags.example.js', '../tagger/tags.js');
};

module.exports = init;
